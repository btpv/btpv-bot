FROM python:3.11.1
WORKDIR /code
ENV FLASK_APP=app.py
ENV FLASK_RUN_HOST=0.0.0.0
RUN pip install --upgrade pip
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt --no-cache
EXPOSE 5000
COPY src/main.py /code/
CMD ["python3","/code/main.py"]