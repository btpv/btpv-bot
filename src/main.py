import json
import zermelo_for_python
import random
import discord
import sqlite3
from discord import app_commands
from discord.ext import commands
bot = commands.Bot(command_prefix=")", intents=discord.Intents.all())
try:
    db = sqlite3.connect("storage/database.sqlite")
except sqlite3.OperationalError:
    db = sqlite3.connect("/storage/database.sqlite")
db.execute("CREATE TABLE IF NOT EXISTS zermelologin (discord_id INTEGER PRIMARY KEY,school STRING NOT NULL,username STRING NOT NULL,password STRING NOT NULL);")
db.commit()
@bot.event
async def on_ready():
    print("Logged in as")
    print(bot.user.name)
    print(bot.user.id)
    print("------")
    try:
        synced = await bot.tree.sync()
        print(f"synced {len(synced)} commands")
    except Exception as e:
        print(e)
@bot.tree.command(name="ip")
@app_commands.describe(user = "user to get the ip address of")
async def ip(integration: discord.Integration,user : discord.User):
    random.seed(user.id)
    await integration.response.send_message(f"{random.randint(0,255)}.{random.randint(0,255)}.{random.randint(0,255)}.{random.randint(0,255)}")
@bot.tree.command(name="base")
@app_commands.describe(base_in = "the input base",base_out= "the output base",number = "the number to convert")
async def base(integration: discord.Integration,base_in:int,base_out:int,number: str):
    a = int(number,base_in)
    didgets = "0123456789abcdefghijklmnopqrstuvwxyz"
    if base_out > len(didgets):
        raise Exception()
    b = ""
    print(a)
    while a > 0 :
        b += didgets[a % base_out]
        a = a // base_out
    b = b[::-1].upper()
    print(b)
    await integration.response.send_message(f"{b}")
@bot.tree.command(name="login",description="login zermelo")
@app_commands.describe(school = "de school naam (het deel voor zportal in de url)",username = "zermelo username",password = "zermelo password")
async def login(interaction: discord.Integration,school:str,username:str,password:str):
    # logout(interaction)
    db.execute("DELETE FROM zermelologin WHERE discord_id=?;",(interaction.user.id,))
    db.execute("INSERT INTO zermelologin (discord_id,school,username,password) VALUES (?,?,?,?);",(interaction.user.id,school,username,password))
    db.commit()
    await interaction.response.send_message(f"ingelogd als {username}",ephemeral=True)
@bot.tree.command(name="rooster",description="zermelo rooster")
@app_commands.describe(user="user om het rooster van te krijgen")
async def rooster(interaction: discord.Interaction,user: None | discord.User):
    if user == None:
        user = interaction.user
    print(user.id)
    cur = db.cursor()
    cur.execute("SELECT school,username,password FROM zermelologin WHERE discord_id=?",(user.id,))
    data = cur.fetchone()
    if data == None:
        await interaction.response.send_message(f"geen zermelo account gevonden voor <@{user.id}> (login met /login)",ephemeral=True)
        return
    school,username,password  = data
    zml = zermelo_for_python.zermelo(school,username,password)
    response = ""
    pstart = ""
    for i in zml.get_schedule().appointments:
        if i.start.strftime("%D") != pstart:
            pstart = i.start.strftime("%D")
            response += f"\n\n{pstart}\n"
        if (not i.cancelled) and i.id != None:
            response += f"{','.join(i.subjects): <4} {','.join(i.teachers): <4} <t:{int(i.start.timestamp())}>-<t:{int(i.end.timestamp())}>\n"
    if response != '':
        await interaction.response.send_message(f"{response}")
    else:
        await interaction.response.send_message(f"<@{user.id}> is free for the whole week\nlucky bastard",allowed_mentions=discord.AllowedMentions.none())
@bot.tree.command(name="logout",description="logout zermelo")
async def logout(interaction: discord.Interaction):
    db.execute("DELETE FROM zermelologin WHERE discord_id=?;",(interaction.user.id,))
    db.commit()
try:
    try:
        with open("storage/config.json") as f:
            config = json.load(f)
    except FileNotFoundError:
        with open("/storage/config.json") as f:
            config = json.load(f)
    bot.run(config["token"])
except Exception as e:
    print(e)
finally:
    print("closing db connection")
    db.close()